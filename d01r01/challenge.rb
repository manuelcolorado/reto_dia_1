require 'gitlab'

def challenge()
  Gitlab.endpoint = 'https://gitlab.com/api/v4'
  Gitlab.private_token = 'zEYhvT2zzvD-Y_NqvP62'

  # Pegue el URI del repositorio creado entre las siguiente comillas dobles
  repository = "git@gitlab.com:manuelcolorado/reto_dia_1.git"

  project = Gitlab.project(repository.split(":")[1].split(".git")[0])

  file = Gitlab.get_file(project.id, "file0.txt", "master")

  !(project.nil? || file.nil?)
end
