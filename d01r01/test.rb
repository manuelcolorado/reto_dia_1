require 'minitest/autorun'
require_relative "challenge"
 
class TestChallenge < MiniTest::Unit::TestCase
  def test_challenge
    assert_equal true, challenge()
  end
end