<div style="text-align: center;">
  <img src="http://academia.devtohack.com/assets/logo-academy-black-c0b4a906430b3143562f01bb94fbf463.svg" alt="Markdownify" width="150">
</div>

Introducción, RVM y Git
=======================

Para completar este reto, realice los siguientes pasos:

- Cree un nuevo repositorio en Gitlab.
- Cree una carpeta local para el reto y enlácela con el repositorio ejecutando los comandos indicados en la página del repositorio en Gitlab.
- Cree un archivo llamado "file0.txt" dentro de la carpeta anterior, haga un commit para este archivo y súbalo (push) al repositorio.
- Pegue el URI del repositorio en la variable "repository" dentro del archivo "challenge.rb" del reto.

Input
-----

- Ninguno

Output
------

- Ninguno

Notas
-----

- Ejecute `bundle install` luego de clonar el repositorio.
- Ejecute `rails test.rb` para correr las pruebas y verifique si tiene errores antes de subir los cambios al repositorio.

Criterio de evaluación
----------------------

- Debe crear un repositorio correctamente.
- Debe hacer commit y push de un archivo correctamente.
- Debe pegar el URI del repositorio en el archivo indicado.

Instrucciones
------------

- Haga un fork del repositorio de este reto.
- Clone el nuevo repositorio en su máquina.
- Resuelva el reto según lo solicitado en el enunciado, tomando en cuenta las notas y la definición de listo del reto.
- Haga commit y push de su código a medida que va avanzando.
- Solicite un merge request del commit final al repositorio del reto cuando haya terminado.
- No modifique los tests.
- Utilice sólo los conocimientos impartidos en clase.

Definición de listo
--------------------

- El reto debe estar terminado en su totalidad.
- El pipeline del reto en Gitlab debe estar aprobado.
- Debe solicitar un merge request al repositorio del reto (forked) para que pueda ser revisado por los mentores.